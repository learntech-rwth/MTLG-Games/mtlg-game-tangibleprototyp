﻿// Fügen Sie Ihren Code hier ein.
// full screen

(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    var isFirstActivation = true;

    var ViewManagement = Windows.UI.ViewManagement;
    var ApplicationViewWindowingMode = ViewManagement.ApplicationViewWindowingMode;
    var ApplicationView = ViewManagement.ApplicationView;

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize your application here.
                console.log(MTLG);
                MTLG.init();
                document.addEventListener('keydown', keyHandler)
            } else {
                // TODO: This application was suspended and then terminated.
                // To create a smooth user experience, restore application state here so that it looks like the app never stopped running.
            }
            args.setPromise(WinJS.UI.processAll());
            ApplicationView.preferredLaunchWindowingMode = ApplicationViewWindowingMode.Auto;
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state that needs to persist across suspensions here.
        // You might use the WinJS.Application.sessionState object, which is automatically saved and restored across suspension.
        // If you need to complete an asynchronous operation before your application is suspended, call args.setPromise().
    };

    function keyHandler(e) {
        if (e.keyCode === 27) {
            var view = ApplicationView.getForCurrentView();
            if (view.isFullScreenMode) {
                console.log("exit fullscreen mode");
                view.exitFullScreenMode();
                ApplicationView.preferredLaunchWindowingMode = ApplicationViewWindowingMode.Auto;
                // The SizeChanged event will be raised when the exit from full-screen mode is complete.
            }
            else {
                console.log("enter fullscreen mode");
                if (view.tryEnterFullScreenMode()) {
                    ApplicationView.preferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen;
                    console.log(view.isFullScreenMode);
                }
            }
        }
    }

    app.start();

})();