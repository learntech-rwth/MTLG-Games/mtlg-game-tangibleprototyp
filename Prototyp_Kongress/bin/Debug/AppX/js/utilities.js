/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-06-14T19:08:37+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-06-21T00:48:39+02:00
 */


"use strict";

var MTLG = (function(m) {

  var utilities = (function() {

    function graph() {
      this.nodes = [];
      this.edges = [];
      this.node_id = 0;
      this.edge_id = 0;
    }

    graph.prototype = {
      addNode: function(value) {
        var new_node;

        function node(node_id) {

          function draggable(e) {
            if (!e.tangible) {
              this.x = e.stageX;
              this.y = e.stageY;
              this.node.updateEdges();
            }
          }

          function graphics() {
            this.kind = "circle"
            this.color = "red";
            this.radius = 30;
            this.width = 60;
            this.height = 60;
            this.shape = new createjs.Shape();
            this.shape.on("pressmove", draggable);
            this.stx = 0;
            this.sty = 0;
          }

          graphics.prototype = {

            setPosition: function(posX, posY) {
              this.shape.x = posX;
              this.shape.y = posY;
            },

            getPosX: function() {
              return this.shape.x;
            },

            getPosY: function() {
              return this.shape.y;
            },

            setColor: function(color) {
              this.color = color;
            },

            setSize: function(width, height, radius) {
              this.width = width;
              this.height = height;
              this.radius = radius;
            },

            setThreshhold: function(x, y) {
              if (x && y) {
                this.stx = x;
                this.sty = y;
              } else {
                if (this.kind === "rectangle") {
                  this.stx = this.width / 2;
                  this.sty = this.height / 2;
                }
                if (this.kind === "circle") {
                  this.stx = 0;
                  this.sty = 0;
                }
              }
            }
          }

          this.id = node_id;
          this.edges = [];
          this.value = value;
          this.graphics = new graphics();
          this.graphics.shape.node = this;
        }

        node.prototype = {
          addEdge: function(edge) {
            this.edges.push(edge);
          },

          rmEdge: function(edge) {
            this.edges = this.edges.filter(function(el) {
              return el.id !== edge.id;
            });
          },

          updateEdges: function() {
            this.edges.forEach(function(e) {
              e.updatePos();
            });
          },

          getGraphicsNode: function() {
            return this.graphics.shape.graphics;
          }
        }

        new_node = new node(this.node_id);
        this.nodes.push(new_node);
        this.node_id = this.node_id + 1;
        return new_node;
      },

      addNodes: function(values) {
        var gr = this;
        try {
          if (!values.length) {
            throw "Use an array of values of notes ";
          } else {
            values.forEach(function(el) {
              gr.addNode(el);
            });
          }
        } catch (err) {
          console.log("In function addNotes: " + err);
          console.log("No node was added.");
        } finally {
          console.log(this.nodes);
        }
      },

      rmNode: function(node) {
        var gr = this;
        node.graphics.shape.graphics.clear();
        node.edges.forEach(function(el) {
          gr.rmEdges(el);
        });
        this.nodes = this.nodes.filter(function(el) {
          return el.id !== node.id;
        });
      },

      addEdge: function(node1, node2) {
        function edge(edge_id) {

          function graphics() {
            this.color = "black";
            this.style = 3;
            this.shape = new createjs.Shape();
            this.startX = 0;
            this.startY = 0;
            this.endX = 0;
            this.endY = 0;
          }

          graphics.prototype = {
            setStart: function(x, y) {
              this.startX = x;
              this.startY = y;
            },

            setEnd: function(x, y) {
              this.endX = x;
              this.endY = y;
            }
          }

          try {
            if (!node1) {
              throw "node1 missing"
            }
            if (!node2) {
              throw "node2 missing"
            }
            this.id = edge_id;
            this.node1 = node1;
            this.node1.addEdge(this);
            this.node2 = node2;
            this.node2.addEdge(this);
            this.graphics = new graphics();
            this.graphics.shape.edge = this; // mapping
          } catch (err) {
            console.log("In function addEdge: " + err);
          }
        }

        edge.prototype = {
          updatePos: function() {
            var g = this.graphics,
              ng1 = this.node1.graphics,
              ng2 = this.node2.graphics;
            g.setEnd(ng2.getPosX() + ng2.stx, ng2.getPosY() + ng2.sty);
            g.setStart(ng1.getPosX() + ng1.stx, ng1.getPosY() + ng1.sty);
            g.shape.graphics.clear();
            g.shape.graphics.beginStroke(g.color).moveTo(g.endX, this.graphics.endY).lineTo(g.startX, g.startY).endStroke();
          }
        }

        this.edges.push(new edge(this.edge_id));
        this.edge_id = this.edge_id + 1;
        return this;
      },

      addEdges: function(edges) {
        var gr = this;
        try {
          if (!edges.length) {
            throw "Use an array of edges [[n0,n1][n1,n2]].";
          } else {
            edges.forEach(function(el) {
              if (edges.length !== 2) {
                throw "An edge has only a start and a end-node [n0,n1].";
              } else {
                gr.addEdge(el[0], el[1]);
              }
            });
          }
        } catch (err) {
          console.log("In function addNotes: " + err);
          console.log("No node was added.");
        } finally {
          console.log(this.nodes);
        }
      },

      rmEdges: function(edge) {
        edge.graphics.shape.graphics.clear();
        this.nodes.forEach(function(el) {
          el.rmEdge(edge);
        });
        this.edges = this.edges.filter(function(el) {
          return el.id !== edge.id;
        });
      },

      getFirstNode: function() {
        if (this.nodes[0]) {
          return this.nodes[0];
        }
        return false;
      },

      getLastNode: function() {
        if (this.nodes[0]) {
          return this.nodes[this.nodes.length - 1];
        }
        return false;
      },

      getFirstEdge: function() {
        if (this.edges[0]) {
          return this.edges[0];
        }
        return false;
      },

      getLastEdge: function() {
        if (this.edges[0]) {
          return this.edges[this.edges.length - 1];
        }
        return false;
      },

      updateEdges: function() {
        this.edges.forEach(function(el, index, array) {
          el.updatePos();
        });
      },

      setAllNodesProperties: function(p, value, graphics) {
        if (p && value) {
          this.nodes.forEach(function(el, index, array) {
            var o = el;
            if (graphics) {
              o = el.graphics;
            }
            Object.keys(o).forEach(function(key, index) {
              if (key === p) {
                o[key] = value;
              }
            });
          });
        }
      },

      setAllEdgesProperties: function(p, value, graphics) {
        if (p && value) {
          this.edges.forEach(function(el, index, array) {
            var o = el;
            if (graphics) {
              o = el.graphics;
            }
            Object.keys(o).forEach(function(key, index) {
              if (key === p) {
                o[key] = value;
              }
            });
          });
        }
      },

      setGraphicsNodes: function() {
        this.nodes.forEach(function(el, index, array) {
          var g = el.graphics;
          g.shape.graphics.clear();
          if (g.kind === "circle") {
            g.shape.graphics.beginFill(g.color).drawCircle(0, 0, g.radius).endFill();
          }
          if (g.kind === "rectangle") {
            g.shape.graphics.beginFill(g.color).drawRect(0, 0, g.width, g.height).endFill();
          }
          g.setThreshhold();
        });
      },

      setGraphicsEdges: function() {
        this.edges.forEach(function(el, index, array) {
          el.graphics.shape.graphics.setStrokeStyle(el.graphics.style);
        });
      },

      addToStage: function() {
        var graphics = [this.edges, this.nodes];
        var gr = this;
        var c;
        graphics.forEach(function(g, index, array) {
          g.forEach(function(el, index, array) {
            if (!el.graphics.onstage) {
              if (!c) { // is an edge
                if (gr.getLastEdge().graphics.shape) {
                  MTLG.gibStage().addChild(el.graphics.shape);
                  MTLG.gibStage().setChildIndex(el.graphics.shape, 0);
                }
              } else { // is a node
                MTLG.gibStage().addChild(el.graphics.shape);
                MTLG.gibStage().setChildIndex(el.graphics.shape, MTLG.gibStage().getNumChildren() - 1);
              }
            }
            el.graphics.onstage = true;
          });
          c = true;
        });
      },

      rmGraph: function() {
        var gr = this;
        this.nodes.forEach(function(el) {
          gr.rmNode(el);
        });
        this.edges.forEach(function() {

        });
        this.node_id = 0;
        this.edge_id = 0;
      },

      updateGraph: function() {
        this.addToStage();
        this.setGraphicsNodes();
        this.setGraphicsEdges();
        this.updateEdges();
      },

      randomEdges: function() {
        var gr = this;
        var random_node;
        this.nodes.forEach(function(el) {
            if (Math.random() < 0.8 && gr.nodes.length > 2) {
            random_node = gr.nodes[Math.floor(Math.random() * (gr.nodes.length)) - 1];
            while (random_node.id === el.id) {
              random_node = gr.nodes[Math.floor(Math.random() * (gr.nodes.length)) - 1];
            }
            gr.addEdge(el, random_node);
          }
        });
      },

      distributeOnStage: function() {
        var x = 50,
          y = 50;
        this.nodes.forEach(function(el, index, array) {
          el.graphics.setPosition(x, y);
          x = x + 100;
          y = y + 100;
        });
        this.updateGraph();
      }
    }

    function init(objects) {
      // fill with inital settings
    }

    m.addModule(init);

    return {
      init: init,
      graph: graph
    }
  })();

  m.utilities = utilities;
  return m;
})(MTLG);
