/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-05-08T17:58:18+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2017-06-19T13:44:04+02:00
 */

var MTLG = function() {
  var stage,
    screenW,
    screenH,
    screenZoll = 27,
    moduleInit,
    prop, optionen;

  function init() {
    stage = new createjs.Stage('c_1');
    window.addEventListener("resize", _resize_canvas);
    MTLG.resize();

    optionen = optionen || {};
    defaults = {
        zoll: 27
    };

    /*
    * Wenn keine Parameter angegeben werden, default Werte benutzen
    */

    for (prop in defaults) {
      if (prop in optionen) {
        continue;
      }
      optionen[prop] = defaults[prop];
    }

    _loadSettings();
  }

  function _loadSettings() {
    if (MTLG.setting_menu) {
      MTLG.setting_menu.init(stage);
      MTLG.setting_menu.start([12.5, 27, 84], _loadModules);
    }
  }

  function _loadModules(zoll) {
    var i;
    optionen.zoll = zoll;

    //Initialize modules
    moduleInit = moduleInit || []; //Make sure this works even if no module was registered
    for (i = 0; i < moduleInit.length; i++) {
      try {
        (moduleInit[i])(optionen);
      } catch (err) {
        console.log("Error calling init function: ");
        console.log(err);
        console.log(moduleInit[i]);
      }
    }

    _lifecycle();
  }

  function _lifecycle() {
      console.log("Lifecycle");
    if (MTLG.game) {
      MTLG.game.init(stage);
    }
  }


  function _getScrW() {
    return screenW;
  }

  function _getScrH() {
    return screenW;
  }

  function _getScrZoll() {
    return screenZoll;
  }

  function _getStage() {
    return stage;
  }

  function _resize_canvas() {
    var canvas = document.getElementsByTagName('canvas'),
      i;

    screenW = window.innerWidth;
    screenH = window.innerHeight;

    for (i = 0; i < canvas.length; i = i + 1) {
      canvas[i].height = screenH;
      canvas[i].width = screenW;
    }
    if (MTLG.game.isStarted()) MTLG.game.resize_game();
    stage.update();
  }

  /*
   * Add an initialization callback for a module.
   * This will be called in MTLG.init().
   */
  function addModule(initCallback) {
    moduleInit = moduleInit || [];
    moduleInit.push(initCallback);
  }


  return {
    init: init,
    resize: _resize_canvas,
    getScreenZoll: _getScrZoll,
    addModule: addModule,
    gibStage: _getStage
  }
}();
